CANalyst-II
===========

CANalyst-II(+) is a USB to CAN Analyzer. The controlcan library is originally developed by
`ZLG ZHIYUAN Electronics`_.

.. note::

   Use of this interface requires the ``ControlCAN.dll`` (Windows) or ``libcontrolcan.so`` vendor library to be placed in the Python working directory.

Bus
---

.. autoclass:: can.interfaces.canalystii.CANalystIIBus


.. _ZLG ZHIYUAN Electronics: http://www.zlg.com/can/can/product/id/42.html
